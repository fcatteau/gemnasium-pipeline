package sbt

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestParse(t *testing.T) {
	types := []string{"small", "big", "duplicates"}
	for _, testType := range types {
		t.Run(testType, func(t *testing.T) {
			b, err := ioutil.ReadFile("expect/" + testType + "/packages.json")
			if err != nil {
				t.Fatal(err)
			}
			want := []parser.Package{}
			err = json.Unmarshal(b, &want)
			if err != nil {
				t.Fatal(err)
			}

			xml, err := os.Open("fixtures/" + testType + "/ivy-report.xml")
			if err != nil {
				t.Fatal(err)
			}

			got, _, err := Parse(xml)
			if err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(got, want) {
				t.Errorf("expected generated packages report for test type %s to be\n%#v\nbut got\n%#v", testType, want, got)
			}
		})
	}
}

func TestVersionParsing(t *testing.T) {
	cases := []struct {
		casename string
		xmlstr   string
		err      error
	}{
		{
			casename: "exact version",
			xmlstr:   `<ivy-report version="1.0"></ivy-report>`,
			err:      nil,
		},
		{
			casename: "mismatch on minor version",
			xmlstr:   `<ivy-report version="1.1"></ivy-report>`,
			err:      parser.ErrWrongFileFormatVersion,
		},
		{
			casename: "mismatch on major version",
			xmlstr:   `<ivy-report version="2.0"></ivy-report>`,
			err:      parser.ErrWrongFileFormatVersion,
		},
	}
	for _, c := range cases {
		t.Run(c.casename, func(t *testing.T) {
			_, _, err := Parse(strings.NewReader(c.xmlstr))
			if err != c.err {
				t.Errorf("unexpected error value, expected %v but got %v", c.err, err)
			}
		})
	}
}
