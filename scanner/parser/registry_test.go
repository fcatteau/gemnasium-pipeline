package parser

import (
	"errors"
	"fmt"
	"io"
	"reflect"
	"testing"
)

// Test_Registry is a combined tests where Register is tested indirectly.
func Test_Registry(t *testing.T) {

	// clear registry
	parsersMu.Lock()
	parsers = make(map[string]Parser)
	parsersMu.Unlock()

	// unsorted list of package types
	pkgTypes := []PackageType{PackageTypeNpm, PackageTypeMaven, PackageTypePackagist, PackageTypeGem, PackageTypeGo}

	// register parsers with duplicates
	for _, pkgType := range pkgTypes {
		name := string(pkgType)
		parse := func(r io.Reader) ([]Package, []Dependency, error) {
			return nil, nil, errors.New("no parser for " + name)
		}
		Register(name+"1", Parser{
			Parse:          parse,
			PackageManager: fmt.Sprintf("package manager 1 for %ss", name),
			PackageType:    pkgType,
			Filenames:      []string{name + "file"},
		})
		Register(name+"2", Parser{
			Parse:          parse,
			PackageManager: fmt.Sprintf("package manager 2 for %ss", name),
			PackageType:    pkgType,
			Filenames:      []string{"package." + name, "manifest." + name},
		})
	}

	t.Run("Lookup", func(t *testing.T) {
		t.Run("found", func(t *testing.T) {
			p := Lookup("manifest.npm")
			want := "package manager 2 for npms"
			if p == nil {
				t.Fatalf("Expected '%s' but got nil", want)
			}
			got := p.PackageManager
			if got != want {
				t.Errorf("Expected '%s' but got '%s'", want, got)
			}
		})
		t.Run("missing", func(t *testing.T) {
			p := Lookup("missing.npm")
			if p != nil {
				t.Errorf("Expected no parser but got '%s'", p.PackageManager)
			}
		})
	})

	t.Run("Parsers", func(t *testing.T) {
		want := []string{"gem1", "gem2", "go1", "go2", "maven1", "maven2", "npm1", "npm2", "packagist1", "packagist2"}
		got := Parsers()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Expected %v but got %v", want, got)
		}
	})

	t.Run("PackageTypes", func(t *testing.T) {
		want := []string{"gem", "go", "maven", "npm", "packagist"}
		got := PackageTypes()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Expected %v but got %v", want, got)
		}
	})
}
