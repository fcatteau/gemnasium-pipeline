package gosum

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestParse(t *testing.T) {
	types := []string{"small", "big", "duplicates", "malformed", "incompatible"}
	for _, testType := range types {
		t.Run(testType, func(t *testing.T) {
			b, err := ioutil.ReadFile("expect/" + testType + "/packages.json")
			if err != nil {
				t.Fatal(err)
			}
			want := []parser.Package{}
			err = json.Unmarshal(b, &want)
			if err != nil {
				t.Fatal(err)
			}

			text, err := os.Open("fixtures/" + testType + "/go.sum")
			if err != nil {
				t.Fatal(err)
			}

			got, _, err := Parse(text)
			if err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(got, want) {
				t.Errorf("expected generated packages report for test type %s to be\n%#v\nbut got\n%#v", testType, want, got)
			}
		})
	}
}
