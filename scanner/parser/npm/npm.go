package npm

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Document represents a node document
type Document struct {
	LockfileVersion int64 `json:"lockfileVersion"`
	DependencyNode
}

// Dependency contains a dependency node
type Dependency struct {
	Version string `json:"version"` // Installed version
	Line    int64  `json:"line"`
	DependencyNode
}

// DependencyNode contains a map of dependencies
type DependencyNode struct {
	Dependencies map[string]Dependency `json:"dependencies"` // Nested dependencies
}

const supportedFileFormatVersion = 1

// Parse scans a npm lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}
	if document.LockfileVersion != supportedFileFormatVersion {
		return nil, nil, parser.ErrWrongFileFormatVersion
	}
	pkgs, err := parseNode(document.DependencyNode)

	if err != nil {
		return nil, nil, err
	}

	return removeDuplicates(pkgs), nil, nil
}

func init() {
	parser.Register("npm", parser.Parser{
		Parse:          Parse,
		PackageManager: "npm",
		PackageType:    parser.PackageTypeNpm,
		Filenames:      []string{"package-lock.json", "npm-shrinkwrap.json"},
	})
}

func parseNode(dn DependencyNode) ([]parser.Package, error) {
	pkgs := []parser.Package{}
	for name, dep := range dn.Dependencies {
		pkgs = append(pkgs, parser.Package{Name: name, Version: dep.Version})
		subpkgs, err := parseNode(dep.DependencyNode)
		if err != nil {
			return nil, err
		}
		pkgs = append(pkgs, subpkgs...)
	}
	return pkgs, nil
}

func removeDuplicates(pkgs []parser.Package) []parser.Package {
	keys := make(map[string]bool)
	list := []parser.Package{}
	for _, pkg := range pkgs {
		key := pkg.Name + "@" + pkg.Version
		if _, found := keys[key]; !found {
			keys[key] = true
			list = append(list, pkg)
		}
	}
	return list
}
