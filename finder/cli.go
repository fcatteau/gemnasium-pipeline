package finder

import (
	"github.com/urfave/cli/v2"
)

// Preset is a set of pre-defined PackageManagers for a particular
// detection configuration
type Preset int

const (
	// PresetGemnasium makes the finder detect projects for the gemnasium analyzer
	PresetGemnasium Preset = iota

	// PresetGemnasiumMaven makes makes the finder detect projects for the gemnasium-maven analyzer
	PresetGemnasiumMaven

	// PresetGemnasiumPython makes makes the finder detect projects for the gemnasium-python analyzer
	PresetGemnasiumPython
)

const (
	flagIgnoredDirs   = "ignored-dirs"
	envVarIgnoredDirs = "SEARCH_IGNORED_DIRS"

	flagIgnoreHiddenDirs   = "ignore-hidden-dirs"
	envVarIgnoreHiddenDirs = "SEARCH_IGNORE_HIDDEN_DIRS"

	flagRequirementsFile   = "pip-requirements-file"
	envVarRequirementsFile = "PIP_REQUIREMENTS_FILE"
)

var defaultIgnoredDirs = cli.NewStringSlice("node_modules", ".bundle", "vendor", ".git")

// Flags generates CLI flags to configure the finder.
// These flags are a subset of the ones defined in the search package of the common library.
func Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringSliceFlag{
			Name:    flagIgnoredDirs,
			EnvVars: []string{envVarIgnoredDirs},
			Usage:   "Directory to be ignored",
			Value:   defaultIgnoredDirs,
		},
		&cli.BoolFlag{
			Name:    flagIgnoreHiddenDirs,
			EnvVars: []string{envVarIgnoreHiddenDirs},
			Usage:   "Ignore hidden directories",
			Value:   true,
		},
	}
}

// NewFinder initializes a new finder for a given preset and CLI context
func NewFinder(c *cli.Context, preset Preset) *Finder {
	return newFinder(config{
		Preset:              preset,
		PipRequirementsFile: c.String(flagRequirementsFile),
		IgnoredDirs:         c.StringSlice(flagIgnoredDirs),
		IgnoreHiddenDirs:    c.Bool(flagIgnoreHiddenDirs),
	})
}

// config configures a finder
type config struct {
	Preset              Preset
	IgnoredDirs         []string
	IgnoreHiddenDirs    bool
	PipRequirementsFile string
}

// newFinder initializes a finder using the given configuration.
// It ensures that options are applied in a consistent manner not matter what the preset is.
func newFinder(cfg config) *Finder {
	f := newFinderWithPreset(cfg)
	f.IgnoredDirs = cfg.IgnoredDirs
	f.IgnoreHiddenDirs = cfg.IgnoreHiddenDirs
	return f
}

// newFinderWithPreset initializes a finder using the preset and preset-specific options.
// Generic options of the configuration are not applied to the new finder.
func newFinderWithPreset(cfg config) *Finder {
	switch cfg.Preset {
	case PresetGemnasium:
		return &Finder{
			Detect: NewDetect(
				PackageManagerConan,
				PackageManagerComposer,
				PackageManagerGo,
				PackageManagerNpm,
				PackageManagerNuget,
				PackageManagerYarn,
				PackageManagerBundler,
			),
			FileTypes:  []FileType{FileTypeLockFile},
			SearchMode: SearchAll,
		}

	case PresetGemnasiumMaven:
		return &Finder{
			Detect: NewDetect(
				PackageManagerMaven,
				PackageManagerGradle,
				PackageManagerSbt,
			),
			FileTypes:  []FileType{FileTypeRequirements},
			SearchMode: SearchSingleTree,
		}

	case PresetGemnasiumPython:
		// prepend PIP_REQUIREMENTS_FILE to pip requirements file if set
		pip := PackageManagerPip
		if filename := cfg.PipRequirementsFile; filename != "" {
			file := File{Filename: filename, FileType: FileTypeRequirements}
			pip.Files = append([]File{file}, pip.Files...)
		}

		return &Finder{
			Detect: NewDetect(
				pip,
				PackageManagerPipenv,
				PackageManagerPoetry,
				// setuptools must be last because pip and pipenv projects
				// might have a setuptools script (setup.py)
				PackageManagerSetuptools,
			),
			FileTypes:  []FileType{FileTypeRequirements},
			SearchMode: SearchSingleDir,
		}

	default:
		return &Finder{Detect: NewDetect()}
	}
}
